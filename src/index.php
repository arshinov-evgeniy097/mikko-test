<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.11.2019
 * Time: 16:28
 */
require_once("vendor/autoload.php");

use App\SalaryHelper;

if (isset($argv[1])) {
    $fileName = $argv[1];

    $salaryHelper = new SalaryHelper();
    $payments = $salaryHelper->getSalaryDates();

    $writer = new \App\Writers\CsvWriter($fileName);
    $writer->save($salaryHelper->getHeader(), $payments);
} else {
    echo 'FAILED: not find filename';
}