<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.11.2019
 * Time: 16:33
 */

namespace App\Writers;

/**
 * Class CsvWriter
 * @package App\Writers
 */
class CsvWriter
{
    /**
     * @var
     */
    private $fileName;
    private $storagePath = __DIR__ . '/../../storage/';
    /**
     * CsvWriter constructor.
     * @param $fileName
     */
    public function __construct($fileName)
    {
        $this->setFileName($fileName);
    }

    /**
     * @param array $header
     * @param array $data
     */
    public function save(array $header, array $data)
    {

        $file = fopen($this->storagePath.$this->fileName, 'w');
        fputcsv($file, $header, ';', '"');

        foreach ($data as $fields) {
            fputcsv($file, $fields, ';', '"');
        }
        fclose($file);
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $this->buildFileName($fileName);
    }

    /**
     * @param $fileName
     * @return string
     */
    private function buildFileName($fileName){
        if (strpos($fileName, ".csv") === false) {
            $fileName .= ".csv";
        }
        return $fileName;
    }
}