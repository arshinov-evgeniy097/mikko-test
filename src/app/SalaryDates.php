<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.11.2019
 * Time: 16:48
 */

namespace App;

use DateTime;
use DateInterval;
use DatePeriod;

/**
 * Class SalaryDates
 * @package App
 */
class SalaryDates
{
    /**
     * @var int
     */
    private $sunday = 0;
    /**
     * @var int
     */
    private $saturday = 6;

    /**
     * @param $lastDay
     * @return string
     */
    public function getBaseDay($lastDay)
    {
        $day = new DateTime($lastDay);
        $weekday = $this->toWeekDay($day);

        switch ($weekday) {
            case $this->saturday:
                $date = $day->modify("-1 day");
                return $date->format("Y-m-d");
                break;
            case $this->sunday:
                $date = $day->modify("-2 day");
                return $date->format("Y-m-d");
                break;
            default:
                return $lastDay;
                break;
        }
    }

    /**
     * @param $lastDay
     * @return string
     */
    public function getBonusDay($lastDay)
    {
        $day = new DateTime($lastDay);
        $weekday = $this->toWeekDay($day);
        switch ($weekday) {
            case $this->saturday:
                $date = $day->modify("+4 day");
                return $date->format("Y-m-d");
                break;
            case $this->sunday:
                $date = $day->modify("+3 day");
                return $date->format("Y-m-d");
                break;
            default:
                return $lastDay;
                break;
        }
    }

    /**
     * @param DateTime $day
     * @return string
     */
    public function toWeekDay(DateTime $day)
    {
        $weekday = $day->format("w");
        return $weekday;
    }

    /**
     * @return DatePeriod
     */
    public function getPeriod()
    {
        $start = new DateTime("today");
        $monthNumber = $start->format("m");
        $remains = $this->getMonthRemains($monthNumber);

        $interval = new DateInterval("P1M");
        $end = new DateTime("today +$remains month");
        $period = new DatePeriod($start, $interval, $end);
        return $period;
    }

    /**
     * @param $monthNumber
     * @return int
     */
    public function getMonthRemains($monthNumber){
        $remains = 12 - $monthNumber + 2;
        return $remains;
    }
}