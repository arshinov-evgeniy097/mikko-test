<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.11.2019
 * Time: 16:48
 */

namespace App;

/**
 * Class SalaryHelper
 * @package App
 */
class SalaryHelper extends SalaryDates
{
    /**
     * @return mixed
     */
    public function getSalaryDates()
    {
        $period = $this->getPeriod();

        foreach ($period as $date) {
            $month = $date->format("M");
            $lastDay = $date->format("Y-m-t");
            $bonusDay = $date->format("Y-m-15");

            $emolument[$month]['month'] = $month;
            $emolument[$month]['base'] = $this->getBaseDay($lastDay);
            $emolument[$month]['bonus'] = $this->getBonusDay($bonusDay);
        }

        $emolument = $this->cleanLastBase($emolument);
        return $emolument;
    }

    /**
     * @param $payments
     * @return mixed
     * because
     * On the 15th of every month bonuses are paid for the previous month, unless that day is a weekend.
     */
    public function cleanLastBase($payments){
        $last = end($payments);
        $payments[$last["month"]]["base"]="";
        return $payments;
    }

    /**
     * @return array
     */
    public function getHeader(){
        $header = ['month', 'base', 'bonus'];
        return $header;
    }
}