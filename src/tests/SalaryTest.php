<?php
use PHPUnit\Framework\TestCase;

class SalaryTest extends TestCase
{
    public function testGetBonusDaySunday()
    {
        $date = "2019-12-15";
        $expectedDay = "2019-12-18";
        $salaryHelper = new \App\SalaryDates();
        $bonusDay = $salaryHelper->getBonusDay($date);
        $this->assertEquals($expectedDay,$bonusDay);
    }

    public function testGetBonusDaySaturday()
    {
        $date = "2020-02-15";
        $trueDate = "2020-02-19";
        $salaryHelper = new \App\SalaryDates();
        $bonusDay = $salaryHelper->getBonusDay($date);
        $this->assertEquals($bonusDay,$trueDate);
    }

    public function testGetBaseDaySaturday(){
        $lastDayOnMonth = "2020-02-29";
        $expectedDay = "2020-02-28";
        $salaryHelper = new \App\SalaryDates();
        $bonusDay = $salaryHelper->getBaseDay($lastDayOnMonth);
        $this->assertEquals($bonusDay, $expectedDay);
    }

    public function testGetBaseDaySunday(){
        $lastDayOnMonth = "2019-06-30";
        $expectedDay = "2019-06-28";
        $salaryHelper = new \App\SalaryDates();
        $bonusDay = $salaryHelper->getBaseDay($lastDayOnMonth);
        $this->assertEquals($bonusDay, $expectedDay);
    }

    public function testGetHeader(){
        $header = ['month', 'base', 'bonus'];
        $salaryHelper = new \App\SalaryHelper();
        $actualHeader = $salaryHelper->getHeader();
        $this->assertEquals($header,$actualHeader);
    }

    public function testGetMonthRemains(){
        $expected = "12";
        $salaryHelper = new \App\SalaryDates();
        $actual = $salaryHelper->getMonthRemains(2);
        $this->assertEquals($expected, $actual);
    }
}